import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import lili from './lili.png'


function App() {
  return (
    <div className="app"><div className="container">
      <div className="card bg-light text-dark w-50 m-auto mt-3">
        <div className="d-flex mb-3">
          <div className="p-4 flex-grow-1  w-25">
           
              <img className="m-auto  " style={{ borderRadius: "0 30px 0 0" }} src={lili} />
            
          </div>
          <div className=" p-4 flex-grow-2 ">
            <div className="d-flex flex-column m-auto">
              <i className="fas fa-user p-3" />
              <i className="fas fa-user p-3" />
              <i className="fas fa-user p-3" />
              <i className="fas fa-user p-3" />
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
}

export default App;
